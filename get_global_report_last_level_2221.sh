#!/usr/bin/bash

PATH_OUTPUT="reports/last_level_2221/"

echo "Reporte de las estructuras del último nivel (conf: k1=2, k2=2, n1=2, pl=1): "

./last_level_global $PATH_OUTPUT"DLWRF_last.txt" $PATH_OUTPUT"DLWRF_snaps_val.txt" $PATH_OUTPUT"DLWRF_logs_val.txt" $PATH_OUTPUT"DLWRF_snaps_sub.txt" $PATH_OUTPUT"DLWRF_logs_sub.txt" > $PATH_OUTPUT"resultados_globales_last_level.txt"
echo $PATH_OUTPUT"DLWRF_last.txt Listo!"

./last_level_global $PATH_OUTPUT"PRES_last.txt" $PATH_OUTPUT"PRES_snaps_val.txt" $PATH_OUTPUT"PRES_logs_val.txt" $PATH_OUTPUT"PRES_snaps_sub.txt" $PATH_OUTPUT"PRES_logs_sub.txt" >> $PATH_OUTPUT"resultados_globales_last_level.txt"
echo $PATH_OUTPUT"PRES_last.txt Listo!"

./last_level_global $PATH_OUTPUT"SPFH_last.txt" $PATH_OUTPUT"SPFH_snaps_val.txt" $PATH_OUTPUT"SPFH_logs_val.txt" $PATH_OUTPUT"SPFH_snaps_sub.txt" $PATH_OUTPUT"SPFH_logs_sub.txt" >> $PATH_OUTPUT"resultados_globales_last_level.txt"
echo $PATH_OUTPUT"SPFH_last.txt Listo!"

echo "Reducción de las listas de frecuencias..."

./reduce_frequency_range $PATH_OUTPUT"DLWRF_snaps_val.txt" 1000 > $PATH_OUTPUT"DLWRF_snaps_val_red.txt"
echo $PATH_OUTPUT"DLWRF_snaps_val_red.txt Listo!"
./reduce_frequency_range $PATH_OUTPUT"PRES_snaps_val.txt" 200000 > $PATH_OUTPUT"PRES_snaps_val_red.txt"
echo $PATH_OUTPUT"PRES_snaps_val_red.txt Listo!"
./reduce_frequency_range $PATH_OUTPUT"DLWRF_logs_val.txt" 1000 > $PATH_OUTPUT"DLWRF_logs_val_red.txt"
echo $PATH_OUTPUT"DLWRF_logs_val_red.txt Listo!"
./reduce_frequency_range $PATH_OUTPUT"PRES_logs_val.txt" 200000 > $PATH_OUTPUT"PRES_logs_val_red.txt"
echo $PATH_OUTPUT"PRES_logs_val_red.txt Listo!"

