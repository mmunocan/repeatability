#include "utilities.hpp"

/*
 * Función encargada de intercalar los bits de 'input' con ceros
 * Función helper de 'get_zorder'
 */
unsigned long interleave_uint32_with_zeros(unsigned int input)  {
    unsigned long word = input;
    word = (word ^ (word << 16)) & 0x0000ffff0000ffff;
    word = (word ^ (word << 8 )) & 0x00ff00ff00ff00ff;
    word = (word ^ (word << 4 )) & 0x0f0f0f0f0f0f0f0f;
    word = (word ^ (word << 2 )) & 0x3333333333333333;
    word = (word ^ (word << 1 )) & 0x5555555555555555;
    return word;
}

/*
 * Función que calcula el código de morton
 */
unsigned long get_zorder(unsigned int n_rows, unsigned int n_cols){
	return interleave_uint32_with_zeros(n_cols) | (interleave_uint32_with_zeros(n_rows) << 1);
}

/*
 * Función que calcula la potencia de 2 más pequeña pero más grande entre 'n_rows' y 'n_cols'
 */
unsigned int get_k(unsigned int n_rows, unsigned int n_cols){
	if(n_rows >= n_cols){
		return  (1 << 32-__builtin_clz(n_rows-1));
	}else{
		return  (1 << 32-__builtin_clz(n_cols-1));
	}
}

/*
 * Función que recibe un informe ASCII de plains_levels generados tras la 
 * construcción de un Tk2-raster que utiliza k2Hrasters subyacentes.
 * 'filename': INPUT nombre y ubicación del archivo
 * 'snapshot_values': OUTPUT Valores de las celdas registradas de los rasters snapshots
 * 'logs_values': OUTPUT Valores de las celdas registradas de los rasters logs
 * 'report': OUTPUT estructura que almacena la información del informe
 * return TRUE si el informe está correctamente estructurado, o
 * FALSE si el informe contiene errores
 */
bool get_report_info(string filename, vector<int> & snapshot_values, vector<int> & logs_values, Report & report){
	
	/************************************
	 * DECLARACION INICIAL DE VARIABLES 
	 ************************************/
	int cant_snapshots = -1;
	int cant_logs = -1;
	int index_value = -1;
	int submatrix_cant_cells = -1;
	int cant_submatrix = -1;
	int submatrix_encoded = -1;
	int plain_vals_cells = -1;
	int cant_cells_1, cant_cells_2;
	int cell_value = -1;
	int submatrix_size = -1;
	
	ifstream file_report(filename);
	
	/************************************
	 * REVISION DE LA LISTA DE SNAPSHOTS
	 ************************************/
	file_report >> cant_snapshots;
	report.n_snapshots = cant_snapshots;
	for(int s = 0; s < cant_snapshots; s++){
		
		/************************************
		 * CHEQUEO DEL VALOR DEL ÍNDICE
	 	 ***********************************/
		file_report >> index_value;
		if(index_value != s){
			cout << "ERROR! el índice s de snapshots no coincide" << endl;
			cout << "Valor esperado: " << s << ", valor recibido: " << index_value << endl;
			return false;
		}
		
		/**************************************************
		 * CHEQUEO DE LOS PARAMETROS GENERALES DEL RASTER
	 	 *************************************************/
		file_report >> submatrix_cant_cells >> cant_submatrix >> submatrix_encoded >> plain_vals_cells;
		if(s == 0){
			submatrix_size = submatrix_cant_cells;
			report.submatrix_size = submatrix_size;
		}else{
			if(submatrix_size != submatrix_cant_cells){
				cout << "ERROR! algo no cuadra en el snapshot " << s << endl;
				cout << "submatrix_size: " << submatrix_size << ", pero se reporta " << submatrix_cant_cells << endl;
				return false;
			}
		}
		
		cant_cells_1 = cant_submatrix * submatrix_cant_cells;
		cant_cells_2 = submatrix_encoded * submatrix_cant_cells + plain_vals_cells;
		if(cant_cells_1 != cant_cells_2){
			cout << "ERROR! algo no cuadra en el snapshot " << s << endl;
			cout << "Mira los valores!!" << endl;
			cout << "submatrix_cant_cells: " << submatrix_cant_cells << ", cant_submatrix: " << cant_submatrix << ", submatrix_encoded: " << submatrix_encoded << ", plain_vals_cells: " << plain_vals_cells << endl;
			return false;
		}
		
		/********************************************
		 * GENERACION DE LA INFORMACIÓN DE UN RASTER
	 	 *******************************************/
		Raster r;
		r.n_submatrix = cant_submatrix;
		r.submatrix_encoded = submatrix_encoded;
		r.submatrix_plains = plain_vals_cells / submatrix_cant_cells;
		
		/********************************************
		 * REVISION DE CADA CELDA DEL RASTER
	 	 *******************************************/
		for(int c = 0; c < cant_cells_1; c++){
			file_report >> cell_value;
			snapshot_values.push_back(cell_value);
			r.cells_values_list.push_back(cell_value);
		}
		
		// Guardar la información del raster en la lista del reporte correspondiente
		report.snapshots_list.push_back(r);
		
	}
	
	/************************************
	 * REVISION DE LA LISTA DE LOGS
	 ************************************/
	file_report >> cant_logs;
	report.n_logs = cant_logs;
	for(int l = 0; l < cant_logs; l++){
		
		/************************************
		 * CHEQUEO DEL VALOR DEL ÍNDICE
	 	 ***********************************/
		file_report >> index_value;
		if(index_value != l){
			cout << "ERROR! el índice l de logs no coincide" << endl;
			cout << "Valor esperado: " << l << ", valor recibido: " << index_value << endl;
			return false;
		}
		
		/**************************************************
		 * CHEQUEO DE LOS PARAMETROS GENERALES DEL RASTER
	 	 *************************************************/
		file_report >> submatrix_cant_cells >> cant_submatrix >> submatrix_encoded >> plain_vals_cells;
		if(submatrix_size != submatrix_cant_cells){
			cout << "ERROR! algo no cuadra en el log " << l << endl;
			cout << "submatrix_size: " << submatrix_size << ", pero se reporta " << submatrix_cant_cells << endl;
			return false;
		}
		
		cant_cells_1 = cant_submatrix * submatrix_cant_cells;
		cant_cells_2 = submatrix_encoded * submatrix_cant_cells + plain_vals_cells;
		if(cant_cells_1 != cant_cells_2){
			cout << "ERROR! algo no cuadra en el log " << l << endl;
			cout << "Mira los valores!!" << endl;
			cout << "submatrix_cant_cells: " << submatrix_cant_cells << ", cant_submatrix: " << cant_submatrix << ", submatrix_encoded: " << submatrix_encoded << ", plain_vals_cells: " << plain_vals_cells << endl;
			return false;
		}
		
		/********************************************
		 * GENERACION DE LA INFORMACIÓN DE UN RASTER
	 	 *******************************************/
		Raster r;
		r.n_submatrix = cant_submatrix;
		r.submatrix_encoded = submatrix_encoded;
		r.submatrix_plains = plain_vals_cells / submatrix_cant_cells;
		
		/********************************************
		 * REVISION DE CADA CELDA DEL RASTER
	 	 *******************************************/
		for(int c = 0; c < cant_cells_1; c++){
			file_report >> cell_value;
			logs_values.push_back(cell_value);
			r.cells_values_list.push_back(cell_value);
		}
		
		// Guardar la información del raster en la lista del reporte correspondiente
		report.log_list.push_back(r);
		
	}
	
	file_report.close();
	return true;
}


/*
 * Función que lee desde disco los rasters temporales en representacion plana y los almacena en un vector
 * 'inputs_file': INPUT nombre del archivo principal que lista los archivos que contienen cada raster
 * 'inputs_path': INPUT nombre de la carpeta en donde se encuentran los archivos que contienen cada raster
 * 'n_elements': INPUT cantidad de rasters que contendra la serie de tiempo raster
 * 'values': OUTPUT vector que almacenará los valores leidos
 * La lectura de cada raster será en row-major, pero el almacenado será en Z-orden
 * Los valores interraster se almacenarán en el orden de los rasters listado en 'inputs_file'
 * return TRUE si la lectura se realizó sin problemas, o
 * FALSE si hubo problemas durante la lectura
 */
bool get_raster_zorder(string inputs_file, string inputs_path, size_t n_elements, vector<int> & values){
	// Correccion de 'inputs_path'
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	
	/***************************
	 * DEFINICIÓN DE VARIABLES
	 ***************************/
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	string raster_filename;
	int value;
	
	/*********************************************
	 * INICIO DE LA LECTURA DEL ARCHIVO PRINCIPAL
	 *********************************************/
	ifstream main_file(inputs_file);
	if(!main_file.is_open()){
		cout <<  inputs_file << " can not be opened" << endl;
		return false;
	}
	
	main_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	size_t side = get_k(n_rows, n_cols);
	size_t double_side = side * side;
	size_t position;
	values.assign(double_side * n_elements, -1);
	
	/*******************************
	 * PROCESAMIENTO DE CADA RASTER
	 *******************************/
	for(size_t rast = 0; rast < n_elements; rast++){
		
		main_file >> raster_filename;
		ifstream raster_file(inputs_path+raster_filename);
		if(!raster_file.is_open()){
			cout << inputs_path << raster_filename << " can not be opened" << endl;
			return false;
		}
		
		/*******************************
	 	 * PROCESAMIENTO DE CADA CELDA
	 	 *******************************/
		for(size_t row = 0; row < n_rows; row++){
			for(size_t col = 0; col < n_cols; col++){
				raster_file.read((char *) (& value), sizeof(int));
				position = rast * double_side + get_zorder(row, col);
				values[position] = value;
			}
		}
		raster_file.close();
			
	} 
	
	main_file.close();
	return true;
}

/*
 * Función que lee desde disco los rasters temporales en representacion plana y los almacena en un vector
 * en row major orden
 * 'inputs_file': INPUT nombre del archivo principal que lista los archivos que contienen cada raster
 * 'inputs_path': INPUT nombre de la carpeta en donde se encuentran los archivos que contienen cada raster
 * 'n_elements': INPUT cantidad de rasters que contendra la serie de tiempo raster
 * 'values': OUTPUT estructura que almacenará los valores leidos
 * 'n_rows': OUTPUT número de filas
 * 'n_cols': OUTPUT número de columnas
 * return TRUE si la lectura se realizó sin problemas, o
 * FALSE si hubo problemas durante la lectura
 */
bool get_rasters_row_major(string inputs_file, string inputs_path, size_t n_elements, vector<int> & values, size_t & n_rows, size_t & n_cols){
	// Correccion de 'inputs_path'
	inputs_path = inputs_path.at(inputs_path.size()-1) != '/' ? inputs_path.append("/") : inputs_path;
	
	/***************************
	 * DEFINICIÓN DE VARIABLES
	 ***************************/
	size_t k1, k2, level_k1, plain_levels;
	string raster_filename;
	int value;
	/*********************************************
	 * INICIO DE LA LECTURA DEL ARCHIVO PRINCIPAL
	 *********************************************/
	ifstream main_file(inputs_file);
	if(!main_file.is_open()){
		cout <<  inputs_file << " can not be opened" << endl;
		return false;
	}
	
	main_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	/*******************************
	 * PROCESAMIENTO DE CADA RASTER
	 *******************************/
	for(size_t rast = 0; rast < n_elements; rast++){
		
		main_file >> raster_filename;
		ifstream raster_file(inputs_path+raster_filename);
		if(!raster_file.is_open()){
			cout << inputs_path << raster_filename << " can not be opened" << endl;
			return false;
		}
		
		
		/***************************************
	 	 * PROCESAMIENTO DE CADA FILA Y COLUMNA
	 	 ***************************************/
		
		for(size_t row = 0; row < n_rows; row++){
			
			for(size_t col = 0; col < n_cols; col++){
				raster_file.read((char *) (& value), sizeof(int));
				
				values.push_back(value);
			}
			
		}
		
		raster_file.close();
			
	} 
	
	main_file.close();
	
	return true;
}






/*
 * Función que calcula las frecuencias de valores.
 * 'values': INPUT array que contiene los valores a procesar
 * 'frequencies': OUTPUT estructura que almacena los valores encontrados y sus respectivas frecuencias
 */
void get_frequencies(const vector<int> & values, map<int, size_t> & frequencies){
	
	for(int val : values){
		
		if(frequencies.count(val)){
			frequencies[val]++;
		}else{
			frequencies[val] = 1;
		}
		
	}
	
}

/*
 * Función que calcula la frecuencia de submatrices. El input contiene las submatrices en row-major.
 * 'values': INPUT array que contiene las submatrices en row-major
 * 'frequencies': OUTPUT estructura que almacena las frecuencias encontradas y sus frecuencias
 * 'side': tamaño del lado de una submatriz
 */
void get_frequencies(const vector<int> & values, map<string, size_t> & frequencies, size_t side, size_t n_elements, size_t n_rows, size_t n_cols){
	
	size_t n_rows_s = ceil(n_rows/side);
	size_t n_cols_s = ceil(n_cols/side);
	size_t row, col;
	
	for(size_t ra = 0; ra < n_elements; ra++){
		for(size_t s_ro = 0; s_ro < n_rows_s; s_ro++){
			for(size_t s_co = 0; s_co < n_cols_s; s_co++){
				// construcción de cada submatriz
				char * submatrix_char;
				string submatrix_str;
				vector<int> submatrix(side*side);
				
				for(size_t r = 0; r < side; r++){
					for(size_t c = 0; c < side; c++){
						row = (s_ro*side) + r;
						col = (s_co*side) + c;
						
						if(row < n_rows && col < n_cols){
							submatrix[r*side + c] = values[ra*(n_rows*n_cols) + (row)*n_cols + (col)];
						}else{
							submatrix[r*side + c] = -1;
						}
					}
				}
				
				submatrix_char = (char*)submatrix.data();
				submatrix_str.assign(submatrix_char, submatrix_char + side*side*sizeof(int));
				
				if(frequencies.count(submatrix_str)){
					frequencies[submatrix_str]++;
				}else{
					frequencies[submatrix_str] = 1;
				}
			}
		}
	}
	
}

/*
 * Función que calcula la frecuencia de submatrices.
 * 'values': INPUT array que contiene las submatrices concatenadas
 * 'frequencies': OUTPUT estructura que almacena las frecuencias encontradas y sus frecuencias
 * 'submatrix_size': tamaño en cantidad de valores de una submatriz
 */
void get_frequencies(const vector<int> & values, map<string, size_t> & frequencies, size_t submatrix_size){
	size_t size = values.size();
	vector<int> submatrix;
	char * submatrix_char;
	string submatrix_str;
	
	for(size_t i = 0; i < size; i += submatrix_size){
		submatrix.assign(submatrix_size, -1);
		for(size_t j = 0; j < submatrix_size; j++){
			submatrix[j] = values[i+j];
		}

		submatrix_char = (char*)submatrix.data();
		submatrix_str.assign(submatrix_char, submatrix_char + submatrix_size*sizeof(int));
		
		if(frequencies.count(submatrix_str)){
			frequencies[submatrix_str]++;
		}else{
			frequencies[submatrix_str] = 1;
		}
	}
}

/*
 * Función que calcula la frecuencias de valores por cada raster por separado.
 * 'report': INPUT la estructura que contiene la información de cada raster
 * 'frequencies_snaps': OUTPUT la lista de distribucion de frecuencias para los rasters snapshots
 * 'frequencies_logs': OUTPUT la lista de distribucion de frecuencias para los rasters logs
 */
void get_frequencies_raster(const Report & report, vector<map<int,size_t>> & frequencies_snaps, vector<map<int,size_t>> & frequencies_logs){
	for(Raster r : report.snapshots_list){
		map<int,size_t> f;
		get_frequencies(r.cells_values_list, f);
		frequencies_snaps.push_back(f);
	}
	
	for(Raster r : report.log_list){
		map<int,size_t> f;
		get_frequencies(r.cells_values_list, f);
		frequencies_logs.push_back(f);
	}
}

/*
 * Función que calcula la frecuencias de submatrices por cada raster por separado.
 * 'report': INPUT la estructura que contiene la información de cada raster
 * 'frequencies_snaps': OUTPUT la lista de distribucion de frecuencias para los rasters snapshots
 * 'frequencies_logs': OUTPUT la lista de distribucion de frecuencias para los rasters logs
 */
void get_frequencies_raster(const Report & report, vector<map<string,size_t>> & frequencies_snaps, vector<map<string,size_t>> & frequencies_logs, size_t submatrix_size){
	for(Raster r : report.snapshots_list){
		map<string,size_t> f;
		get_frequencies(r.cells_values_list, f, submatrix_size);
		frequencies_snaps.push_back(f);
	}
	
	for(Raster r : report.log_list){
		map<string,size_t> f;
		get_frequencies(r.cells_values_list, f, submatrix_size);
		frequencies_logs.push_back(f);
	}
}

/*
 * Función que revisa que la distribución de frecuencias sea correcta.
 * Chequea la función 'get_frequencies'
 * 'frequencies': INPUT estructura que almacena las frecuencias por cada palabra
 * 'cant_vals': INPUT indica cuántos valores debieran ser en total (sumatorias de las frecuencias)
 * return TRUE si la distribución es correcta
 * FALSE si faltaron/sobraron valores en el conteo
 */
bool check_frequencies(const map<int,size_t> & frequencies, size_t cant_vals){
	size_t n_values = 0;
	
	for (auto const& it : frequencies){
		n_values += it.second;
	}
	
	return n_values == cant_vals;
}

/*
 * Función que revisa que la distribución de frecuencias sea correcta.
 * Chequea la función 'get_frequencies'
 * 'frequencies': INPUT estructura que almacena las frecuencias por cada palabra
 * 'cant_vals': INPUT indica cuantas submatrices debieran ser en total (sumatorias de las frecuencias)
 * return TRUE si la distribución es correcta
 * FALSE si faltaron/sobraron valores en el conteo
 */
bool check_frequencies(const map<string,size_t> & frequencies, size_t cant_vals){
	size_t n_values = 0;
	
	for (auto const& it : frequencies){
		n_values += it.second;
	}
	
	return n_values == cant_vals;
}

/*
 * Función que obtiene la cantidad de valores únicos promedio por grupo de distribución de frecuencias
 * 'frequencies': INPUT lista de distribuciones de frequencias de valores
 * RETURN el promedio del tamaño de las distribuciones de frecuencias contenidos en 'frequencies'
 */
size_t get_uniques_values_average(const vector<map<int,size_t>> & frequencies){
	size_t result = 0;
	
	for(map<int,size_t> f : frequencies){
		result += f.size();
	}
	
	return result / frequencies.size();
}

/*
 * Función que obtiene la cantidad de valores únicos promedio por grupo de distribución de frecuencias
 * 'frequencies': INPUT lista de distribuciones de frequencias de submatrices
 * RETURN el promedio del tamaño de las distribuciones de frecuencias contenidos en 'frequencies'
 */
size_t get_uniques_values_average(const vector<map<string,size_t>> & frequencies){
	size_t result = 0;
	
	for(map<string,size_t> f : frequencies){
		result += f.size();
	}
	
	return result / frequencies.size();
}

/*
 * Funciones de ordenamiento
 */
bool sortbysec_values(const pair<int,int> & a, const pair<int,int> & b){
	return (a.second > b.second);
}
bool sortbysec_submatrices(const pair<string,int> & a, const pair<string,int> & b){
	return (a.second > b.second);
}

/*
 * Función que ordena los valores de acuerdo a su frecuencia descendente
 * 'frequencies': INPUT lista de valores con sus frecuencias. Están ordenadas según sus valores ascendentemente.
 * 'vector_sorted': OUTPUT lista de valores con sus frecuencias ordenadas
 */
void sort_by_frequencies(const map<int,size_t> & frequencies, vector<pair<int,size_t>> & vector_sorted){
	
	for (auto const& it : frequencies){
		vector_sorted.push_back(make_pair(it.first, it.second));
	}
	
	sort(vector_sorted.begin(), vector_sorted.end(), sortbysec_values);
}

/*
 * Función que ordena las submatrices de acuerdo a su frecuencia descendente
 * 'frequencies': INPUT lista de submatrices con sus frecuencias.
 * 'vector_sorted': OUTPUT lista de valores con sus frecuencias ordenadas.
 */
void sort_by_frequencies(const map<string,size_t> & frequencies, vector<pair<string,size_t>> & vector_sorted){
	
	for (auto const& it : frequencies){
		vector_sorted.push_back(make_pair(it.first, it.second));
	}
	
	sort(vector_sorted.begin(), vector_sorted.end(), sortbysec_submatrices);
}

void save_frequencies(const vector<map<int,size_t>> & frequencies, string output_path){
	size_t size = frequencies.size();
	string filename;
	
	for(size_t i = 0; i < size; i++){
		filename = output_path + to_string(i) + ".txt";
		save_frequencies(frequencies[i], filename);
	}
	
}

/*
 * Función que almacena en un archivo la lista de valores con sus respectivas frecuencias.
 * La lista está ordenada de acuerdo a los valores ascendentemente ('frequencies' viene así)
 * 'frequencies': INPUT lista de valores con sus frecuencias.
 * 'output_filename': nombre del archivo de salida
 */
void save_frequencies(const map<int,size_t> & frequencies, string output_file){
	ofstream output(output_file);
	
	for (auto const& it : frequencies){
		output << it.first << " " << it.second << endl;
	}
	
	output.close();
	
}

/*
 * Función que almacena en un archivo la lista de submatrices con sus respectivas frecuencias.
 * La lista está ordenada de acuerdo a las frecuencias descendentemente ('vector_sorted' viene así)
 * 'vector_sorted': INPUT lista de submatrices con sus frecuencias.
 * 'output_filename': nombre del archivo de salida
 */
void save_frequencies(const vector<pair<string,size_t>> & vector_sorted, string output_file){
	ofstream output(output_file);
	
	for (auto const& it : vector_sorted){
		output << print_submatrix(it.first) << " " << it.second << endl;
	}
	
	output.close();
}

/*
 * Función que calcula la entropía de orden cero (H_0) de valores
 * 'frequencies': INPUT estructura que almacena las frecuencias de valores
 * 'cant_vals': indica cuantos valores debieran ser en total (sumatorias de las frecuencias)
 */
double get_entropy(const map<int,size_t> & frequencies, size_t cant_vals){
	double entropy = 0.0;
	double prob = 0.0;
	
	for (auto const& it : frequencies){
		prob = (double)it.second / (double)cant_vals;
		
		entropy += (prob * log2(prob));
		
	}
	
	return -entropy;
}

/*
 * Función que calcula la entropía de orden cero (H_0) de submatrices
 * 'frequencies': INPUT estructura que almacena las frecuencias de submatrices
 * 'cant_vals': indica cuantas submatrices debieran ser en total (sumatorias de las frecuencias)
 */
double get_entropy(const map<string,size_t> & frequencies, size_t cant_vals){
	double entropy = 0.0;
	double prob = 0.0;
	
	for (auto const& it : frequencies){
		prob = (double)it.second / (double)cant_vals;
		
		entropy += (prob * log2(prob));
		
	}
	
	return -entropy;
}

/*
 * Función que calcula la entropía de orden cero (H_0) promedio por cada raster
 * 'frequencies': INPUT la estructura que contiene la distribucion de frecuencias por cada raster
 */
double get_entropy_average(const vector<map<int,size_t>> & frequencies){
	size_t cant;
	double entropy = 0.0;
	
	for(map<int,size_t> f: frequencies){
		cant = count_values(f);
		entropy += get_entropy(f, cant);
	}
	
	return entropy / frequencies.size();
}

/*
 * Función que calcula la entropía de orden cero (H_0) promedio por cada raster
 * 'frequencies': INPUT la estructura que contiene la distribucion de frecuencias por cada raster
 */
double get_entropy_average(const vector<map<string,size_t>> & frequencies){
	size_t cant;
	double entropy = 0.0;
	
	for(map<string,size_t> f: frequencies){
		cant = count_submatrices(f);
		entropy += get_entropy(f, cant);
	}
	
	return entropy / frequencies.size();
}

/*
 * Función que recibe una submatriz y la transforma en su versión legible.
 */
string print_submatrix(string submatrix_str){
	int * submatrix_int = (int*)submatrix_str.c_str();
	string output = "";
	size_t size = submatrix_str.size()/sizeof(int);
	
	for(size_t i = 0; i < size; i++){
		output += "[" + to_string(submatrix_int[i]) + "]";
	}
	
	return output;
}

/*
 * Función que cuenta los valores contenidos en la lista de frecuencias.
 * 'frequencies': INPUT estructura que almacena las frecuencias de valores
 */
size_t count_values(const map<int,size_t> & frequencies){
	size_t n_values = 0;
	
	for (auto const& it : frequencies){
		n_values += it.second;
	}
	
	return n_values;
}

/*
 * Función que cuenta las submatrices contenidas en la lista de frecuencias.
 * 'frequencies': INPUT estructura que almacena las frecuencias de submatrices
 */
size_t count_submatrices(const map<string,size_t> & frequencies){
	size_t n_values = 0;
	
	for (auto const& it : frequencies){
		n_values += it.second;
	}
	
	return n_values;
}

/*
 * Función que obtiene el tamaño teórico en bytes que requiere representar una lista de submatrices
 * basado en el uso de un diccionario global.
 * También imprime la cantidad de submatrices únicas seleccionadas
 * 'frequencies': INPUT estructura que almacena las frecuencias de submatrices
 * 'side': cantidad de celdas que contiene la submatriz
 * 'entropy_values': la entropía de valores de todo el conjunto (considerando las celdas como palabras independientes)
 * 'entropy_submatrix': la entropía de las submatrices
 */
size_t get_size_representation_global(const map<string,size_t> & frequencies, size_t side, double entropy_values, double entropy_submatrix){
	size_t total_size = 0;
	size_t size_encode, size_plain;
	
	size_t subs_al_diccionario = 0;
	size_t total_subs_rep = 0;
	
	for (auto const& it : frequencies){
		size_plain = it.second * side * entropy_values;
		size_encode = (side * sizeof(int) * 8) + it.second * entropy_submatrix;
		
		if(size_encode <= size_plain){
			total_size += size_encode;
			
			subs_al_diccionario++;
			total_subs_rep += it.second;
			
		}else{
			total_size += size_plain;
		}
	}
	if(subs_al_diccionario > 0){
		cout << "Se han elegido " << subs_al_diccionario << " submatrices únicas de " << frequencies.size() << ", afectando un total de " << total_subs_rep << " submatrices " << endl;
	}
	
	return total_size / 8;
}

/*
 * Función que obtiene el tamaño teórico en bytes que requiere representar una lista de submatrices
 * basado en el uso de un diccionario por raster.
 * También imprime la cantidad de submatrices únicas seleccionadas
 * 'frequencies': INPUT estructura que almacena las frecuencias de submatrices
 * 'side': cantidad de celdas que contiene la submatriz
 * 'entropy_values': la entropía de valores de todo el conjunto (considerando las celdas como palabras independientes)
 * 'entropy_submatrix': la entropía de las submatrices
 */
size_t get_size_representation_per_raster(const vector<map<string,size_t>> & frequencies, const vector<map<int,size_t>> & frequencies_vals, size_t side){
	size_t total = 0;
	double entropy_values, entropy_submatrix;
	size_t total_values, total_submatrix;
	size_t n_elements = frequencies.size();
	
	for(size_t i = 0; i < n_elements; i++){
		total_values = count_values(frequencies_vals[i]);
		total_submatrix = count_submatrices(frequencies[i]);
		
		entropy_values = get_entropy(frequencies_vals[i], total_values);
		entropy_submatrix = get_entropy(frequencies[i], total_submatrix);
		
		total += get_size_representation_global(frequencies[i], side, entropy_values, entropy_submatrix);
	}
	
	return total;
	
}