#include <iostream>
#include <fstream>
#include <vector>

#include "utilities.hpp"

using namespace std;

void generate_dataset(string path_name, string base_name, size_t raster_size, size_t cant_rasters){
	
	/***************************************************/
	/************ PREPARACION DE VARIABLES *************/
	/***************************************************/
	size_t cells_per_raster = raster_size * raster_size;
	int value = 0;
	size_t position;
	string file_raster_name = "";
	string file_main_name = path_name + base_name + ".txt";
	vector<vector<int>> datas(cant_rasters, vector<int>(cells_per_raster, -1));
	
	/*************************************************/
	/************ GENERACION DEL DATASET *************/
	/*************************************************/
	
	for(size_t raster = 0; raster < cant_rasters; raster++){
		for(size_t row = 0; row < raster_size; row++){
			for(size_t col = 0; col < raster_size; col++){
				position = get_zorder(row, col);
				datas[raster][position] = value;
				value++;
			}
		}
	}
	
	/*************************************************/
	/************** GUARDADO DEL DATASET *************/
	/*************************************************/
	
	ofstream main_file(file_main_name);
	for(size_t raster = 0; raster < cant_rasters; raster++){
		file_raster_name = base_name + "-" + to_string(raster) + ".bin";
		main_file << file_raster_name << endl;
		file_raster_name = path_name + file_raster_name;
		ofstream file_raster(file_raster_name);
		
		for(size_t i = 0; i < cells_per_raster; i++){
			value = datas[raster][i];
			file_raster.write((char *) (& value), sizeof(int));
		}
		
		file_raster.close();
	}
	
	main_file.close();
}

bool check_dataset(string path_name, string base_name, size_t raster_size, size_t cant_rasters){
	/***************************************************/
	/************ PREPARACION DE VARIABLES *************/
	/***************************************************/
	size_t cells_per_raster = raster_size * raster_size;
	int value_expected = 0;
	int value_received;
	size_t position;
	bool result = true;
	string file_raster_name = "";
	string file_main_name = path_name + base_name + ".txt";
	vector<vector<int>> datas(cant_rasters, vector<int>(cells_per_raster, -1));
	
	/***************************************************/
	/************ RECEPCION DE LOS VALORES *************/
	/***************************************************/
	ifstream main_file(file_main_name);
	for(size_t raster = 0; raster < cant_rasters; raster++){
		main_file >> file_raster_name;
		file_raster_name = path_name + file_raster_name;
		ifstream file_raster(file_raster_name);
		
		for(size_t i = 0; i < cells_per_raster; i++){
			file_raster.read((char *) (& value_received), sizeof(int));
			datas[raster][i] = value_received;
		}
		
		file_raster.close();
	}
	main_file.close();
	
	/******************************************************/
	/************ COMPROBACION DE LOS VALORES *************/
	/******************************************************/
	
	for(size_t raster = 0; raster < cant_rasters; raster++){
		for(size_t row = 0; row < raster_size; row++){
			for(size_t col = 0; col < raster_size; col++){
				position = get_zorder(row, col);
				value_received = datas[raster][position];
				
				if(value_received != value_expected){
					cout << "ERROR! row: " << row << ", col: " << col << ", raster: " << raster << endl;
					cout << "Value expected: " << value_expected << ", value received: " << value_received << endl;
					result = false;
				}
				
				value_expected++;
			}
		}
	}
	
	return result;
}

int main(int argc, char ** argv){
	
	string path_name = "data-test/";
	string base_name = "MNOP";
	size_t raster_size = 32;
	size_t cant_rasters = 16;
	
	generate_dataset(path_name, base_name, raster_size, cant_rasters);
	bool result = check_dataset(path_name, base_name, raster_size, cant_rasters);
	
	if(result){
		cout << "EL DATASET SE GENERO CORRECTAMENTE!!!" << endl;
	}else{
		cout << "HUBO ERRORES EN EL DATASET GENERADO" << endl;
	}
	
	return 0;
}