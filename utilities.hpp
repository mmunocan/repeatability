/*
 * Funciones de utilidades y estructuras usadas en todo el proceso
 */


#ifndef UT_HPP
#define UT_HPP

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <map>
#include <utility>
#include <algorithm>

using namespace std;

/* 
 * Almacena la información de los últimos niveles definidos como plains_values de un k2Hraster
 */

struct Raster{
	size_t n_submatrix;					// Cantidad total de submatrices
	size_t submatrix_encoded;			// Cantidad de submatrices elegidas para ser codificadas
	size_t submatrix_plains;			// Cantidad de submatrices elegidas para ser planas
	vector<int> cells_values_list;		// Lista de celdas de valores
};

/* 
 * Almacena la información del reporte los últimos niveles definidos como plains_levels de los k2Hrasters 
 * subyacentes obtenidos del Tk2-rasters heurístico generado al comprimir un raster
 */

struct Report{
	size_t n_snapshots;					// Cantidad de snapshots
	size_t n_logs;						// Cantidad de logs
	size_t submatrix_size;				// Tamaño de la submatriz (en número de celdas)
	vector<Raster> snapshots_list;		// Lista de rasters snapshots
	vector<Raster> log_list;			// Lista de rasters logs
};


/*
 * Funciones relativas a Z-Orden
 */
unsigned long get_zorder(unsigned int n_rows, unsigned int n_cols);
unsigned int get_k(unsigned int n_rows, unsigned int n_cols);

/*
 * Funciones relativas a la lectura y representación de inputs
 */
bool get_report_info(string filename, vector<int> & snapshot_values, vector<int> & logs_values, Report & report);
bool get_raster_zorder(string inputs_file, string inputs_path, size_t n_elements, vector<int> & values);
bool get_rasters_row_major(string inputs_file, string inputs_path, size_t n_elements, vector<int> & values, size_t & n_rows, size_t & n_cols);



/*
 * Funciones relativas al procesamiento de rasters y vectores
 */

void get_frequencies(const vector<int> & values, map<int, size_t> & frequencies);
void get_frequencies(const vector<int> & values, map<string, size_t> & frequencies, size_t side, size_t n_elements, size_t n_rows, size_t n_cols);

void get_frequencies(const vector<int> & values, map<string, size_t> & frequencies, size_t submatrix_size);

void get_frequencies_raster(const Report & report, vector<map<int,size_t>> & frequencies_snaps, vector<map<int,size_t>> & frequencies_logs);
void get_frequencies_raster(const Report & report, vector<map<string,size_t>> & frequencies_snaps, vector<map<string,size_t>> & frequencies_logs, size_t submatrix_size);

bool check_frequencies(const map<int,size_t> & frequencies, size_t cant_vals);
bool check_frequencies(const map<string,size_t> & frequencies, size_t cant_vals);

size_t get_uniques_values_average(const vector<map<int,size_t>> & frequencies);
size_t get_uniques_values_average(const vector<map<string,size_t>> & frequencies);

void sort_by_frequencies(const map<int,size_t> & frequencies, vector<pair<int,size_t>> & vector_sorted);
void sort_by_frequencies(const map<string,size_t> & frequencies, vector<pair<string,size_t>> & vector_sorted);

void save_frequencies(const vector<map<int,size_t>> & frequencies, string output_path);
void save_frequencies(const map<int,size_t> & frequencies, string output_file);
void save_frequencies(const vector<pair<string,size_t>> & vector_sorted, string output_file);

double get_entropy(const map<int,size_t> & frequencies, size_t cant_vals);
double get_entropy(const map<string,size_t> & frequencies, size_t cant_vals);

double get_entropy_average(const vector<map<int,size_t>> & frequencies);
double get_entropy_average(const vector<map<string,size_t>> & frequencies);

string print_submatrix(string submatrix_str);

size_t count_values(const map<int,size_t> & frequencies);
size_t count_submatrices(const map<string,size_t> & frequencies);

size_t get_size_representation_global(const map<string,size_t> & frequencies, size_t side, double entropy_values, double entropy_submatrix);

size_t get_size_representation_per_raster(const vector<map<string,size_t>> & frequencies, const vector<map<int,size_t>> & frequencies_vals, size_t side);


#endif