#!/usr/bin/bash

PATH="reports/last_level_4242/"

echo "Reporte de las estructuras del último nivel (conf: k1=4, k2=2, n1=4, pl=2): "

echo "Generación de los archivos por cada raster..."

./last_level_raster $PATH"DLWRF_last.txt" $PATH"DLWRF/" > $PATH"resultados_average_last_level.txt"
./last_level_raster $PATH"PRES_last.txt" $PATH"PRES/" >> $PATH"resultados_average_last_level.txt"
./last_level_raster $PATH"SPFH_last.txt" $PATH"SPFH/" >> $PATH"resultados_average_last_level.txt"

echo "Listo!"

echo "Reducción de lista de frecuencias DLWRF..."
echo "-- Snaps:"

I=0
NOMBRE=$PATH"DLWRF/snaps"$I".txt"
while [ -f $NOMBRE  ]
do
	NOMBRE_RED=$PATH"DLWRF/snaps"$I"_red.txt"
	./reduce_frequency_range $NOMBRE 1000 > $NOMBRE_RED
	echo $NOMBRE_RED" listo!"
	((I++))
	NOMBRE=$PATH"DLWRF/snaps"$I".txt"
done

echo "-- Logs:"

I=0
NOMBRE=$PATH"DLWRF/logs"$I".txt"
while [ -f $NOMBRE  ]
do
	NOMBRE_RED=$PATH"DLWRF/logs"$I"_red.txt"
	./reduce_frequency_range $NOMBRE 1000 > $NOMBRE_RED
	echo $NOMBRE_RED" listo!"
	((I++))
	NOMBRE=$PATH"DLWRF/logs"$I".txt"
done

echo "Listo!"

echo "Reducción de lista de frecuencias PRES..."
echo "-- Snaps:"

I=0
NOMBRE=$PATH"PRES/snaps"$I".txt"
while [ -f $NOMBRE  ]
do
	NOMBRE_RED=$PATH"PRES/snaps"$I"_red.txt"
	./reduce_frequency_range $NOMBRE 200000 > $NOMBRE_RED
	echo $NOMBRE_RED" listo!"
	((I++))
	NOMBRE=$PATH"PRES/snaps"$I".txt"
done

echo "-- Logs:"

I=0
NOMBRE=$PATH"PRES/logs"$I".txt"
while [ -f $NOMBRE  ]
do
	NOMBRE_RED=$PATH"PRES/logs"$I"_red.txt"
	./reduce_frequency_range $NOMBRE 200000 > $NOMBRE_RED
	echo $NOMBRE_RED" listo!"
	((I++))
	NOMBRE=$PATH"PRES/logs"$I".txt"
done

echo "Listo!"


