#!/usr/bin/bash

PATH_OUTPUT="reports/last_level_2221/"

echo "Generación de las gráficas de valores"

echo "-- snaps"
python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_snaps_val_red.txt" "DLWRF last level snapshots values frequencies" $PATH_OUTPUT"DLWRF_SNAPS.png" 0
echo $PATH_OUTPUT"DLWRF_SNAPS.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_snaps_val_red.txt" "PRES raster last level snapshots values frequencies" $PATH_OUTPUT"PRES_SNAPS.png" 0
echo $PATH_OUTPUT"PRES_SNAPS.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_snaps_val.txt" "SPFH raster last level snapshots values frequencies" $PATH_OUTPUT"SPFH_SNAPS.png" 0
echo $PATH_OUTPUT"SPFH_SNAPS.png Listo!"

echo "-- logs"
python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_logs_val_red.txt" "DLWRF last level logs values frequencies" $PATH_OUTPUT"DLWRF_LOGS.png" 0
echo $PATH_OUTPUT"DLWRF_LOGS.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_logs_val_red.txt" "PRES raster last level logs values frequencies" $PATH_OUTPUT"PRES_LOGS.png" 0
echo $PATH_OUTPUT"PRES_LOGS.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_logs_val.txt" "SPFH raster last level logs values frequencies" $PATH_OUTPUT"SPFH_LOGS.png" 0
echo $PATH_OUTPUT"SPFH_LOGS.png Listo!"

echo "Generación de las gráficas de submatrices"

echo "-- snaps"
python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_snaps_sub.txt" "DLWRF last level snapshots submatrices frequencies" $PATH_OUTPUT"DLWRF_SNAPS_SUB.png" 1
echo $PATH_OUTPUT"DLWRF_SNAPS_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_snaps_sub.txt" "PRES raster last level snapshots submatrices frequencies" $PATH_OUTPUT"PRES_SNAPS_SUB.png" 1
echo $PATH_OUTPUT"PRES_SNAPS_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_snaps_sub.txt" "SPFH raster last level snapshots submatrices frequencies" $PATH_OUTPUT"SPFH_SNAPS_SUB.png" 1
echo $PATH_OUTPUT"SPFH_SNAPS_SUB.png Listo!"

echo "-- logs"
python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_logs_sub.txt" "DLWRF last level logs submatrices frequencies" $PATH_OUTPUT"DLWRF_LOGS_SUB.png" 1
echo $PATH_OUTPUT"DLWRF_LOGS_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_logs_sub.txt" "PRES raster last level logs submatrices frequencies" $PATH_OUTPUT"PRES_LOGS_SUB.png" 1
echo $PATH_OUTPUT"PRES_LOGS_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_logs_sub.txt" "SPFH raster last level logs submatrices frequencies" $PATH_OUTPUT"SPFH_LOGS_SUB.png" 1
echo $PATH_OUTPUT"SPFH_LOGS_SUB.png Listo!" 