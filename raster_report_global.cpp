/* 
 * Tras la lectura de los rasters en plano, obtiene cierta información de relevancia
 * relativa a la frecuencia de valores, submatrices y entropías
 */

#include <iostream>
#include <vector>
#include <map>
#include <iomanip>
#include <cmath>


#include "utilities.hpp"

#define SIDE 8

using namespace std;

int main(int argc, char ** argv){
	if(argc != 6){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> <freq_val_filename> <freq_sub_filename> "<<endl;
		return -1;
	}
	
	/*************************************
	 * RECEPCIÓN DE PARAMETROS DE ENTRADA
	 *************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	string freq_val_filename = argv[4];
	string freq_sub_filename = argv[5];
	
	vector<int> values;
	size_t n_rows, n_cols, cant_values, cant_submatrices;
	if(!get_rasters_row_major(inputs_file, inputs_path, n_elements, values, n_rows, n_cols)){
		cout << "ERROR! DURANTE LA LECTURA DE LOS ARCHIVOS DE ENTRADA" << endl;
		return -1;
	}
	
	cant_values = n_elements * n_rows * n_cols;
	cant_submatrices = n_elements * ceil(n_rows/SIDE) * ceil(n_cols/SIDE);
	
	map<int,size_t> freq_values;
	map<string,size_t> freq_submatrix;
	get_frequencies(values, freq_values);
	get_frequencies(values, freq_submatrix, SIDE, n_elements, n_rows, n_cols);
	
	if(!check_frequencies(freq_values, cant_values)){
		cout << "ERROR! al obtener la frecuencia de los valores unicos sobre el raster" << endl;
		return -1;
	}
	if(!check_frequencies(freq_submatrix, cant_submatrices)){
		cout << "ERROR! al obtener la frecuencia de las submatrices únicas sobre el raster" << endl;
		return -1;
	}
	
	size_t submatrices_per_raster = cant_submatrices / n_elements;
	
	vector<pair<int, size_t>> freq_sorted;
	vector<pair<string, size_t>> freq_submatrix_sorted;
	sort_by_frequencies(freq_values, freq_sorted);
	sort_by_frequencies(freq_submatrix, freq_submatrix_sorted);
	
	save_frequencies(freq_values, freq_val_filename);
	save_frequencies(freq_submatrix_sorted, freq_sub_filename);
	
	double entropy_values = get_entropy(freq_values, cant_values);
	double entropy_submatrices = get_entropy(freq_submatrix, cant_submatrices);
	
	
	cout << "Dimensiones: [" << n_elements << " X " << n_rows << " X " << n_cols << "]" << endl;
	cout << "Existen " << freq_values.size() << " valores diferentes" << endl;
	cout << "Existen " << freq_submatrix.size() << " submatrices diferentes" << endl;
	cout << "Encontré " << cant_submatrices << " submatrices de " << SIDE << " X " << SIDE << " (" << submatrices_per_raster << " submatrices por raster)" << endl;
	
	cout << "El archivo con las frecuencias de valores fue guardado en " << freq_val_filename << endl;
	cout << "El archivo con las frecuencias de submatrices fue guardado en " << freq_sub_filename << endl;
	
	cout << "Entropía de valores: " << fixed << setprecision(4) << entropy_values << endl;
	cout << "Entropía de submatrices: " << fixed << setprecision(4) << entropy_submatrices << endl;
	
	cout << "10 valores más frecuentes: " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_sorted.size())
			cout << freq_sorted[i].first << ": (" << freq_sorted[i].second << " apariciones)" << endl;
	}
	
	cout << "10 submatrices más frecuentes: " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_submatrix_sorted.size())
			cout << print_submatrix(freq_submatrix_sorted[i].first) << ": (" << freq_submatrix_sorted[i].second << " apariciones)" << endl;
	}
	
	
	return 0;
}
