#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#include "utilities.hpp"

using namespace std;

int main(int argc, char ** argv){
	if(argc != 6){
		cout<<"usage: " << argv[0] << " <filename> <freq_snapshots_filename> <freq_logs_filename> <freq_snapshots_filename_submatrix> <freq_logs_filename_submatrix>"<<endl;
		return -1;
	}
	
	string filename = argv[1];
	string freq_snapshots_filename = argv[2];
	string freq_logs_filename = argv[3];
	string freq_snapshots_filename_submatrix = argv[4];
	string freq_logs_filename_submatrix = argv[5];
	
	Report report;
	vector<int> snapshots_values, logs_values;
	if(!get_report_info(filename, snapshots_values, logs_values, report)){
		cout << "EL INFORME CONTIENE ERRORES" << endl;
		return -1;
	}
	
	size_t submatrix_size = report.submatrix_size;
	size_t cant_snapshots = report.n_snapshots;
	size_t cant_logs = report.n_logs;
	size_t snaps_cant_values = snapshots_values.size();
	size_t logs_cant_values = logs_values.size();
	size_t snaps_cant_submatrix = snaps_cant_values / submatrix_size;
	size_t logs_cant_submatrix = logs_cant_values / submatrix_size;
	
	map<int,size_t> freq_values_snap, freq_values_logs;
	map<string, size_t> freq_submatrix_snap, freq_submatrix_logs;
	vector<pair<int, size_t>> freq_sorted_snaps, freq_sorted_logs;
	vector<pair<string, size_t>> freq_sorted_snaps_submatrix, freq_sorted_logs_submatrix;
	
	get_frequencies(snapshots_values, freq_values_snap);
	get_frequencies(logs_values, freq_values_logs);
	get_frequencies(snapshots_values, freq_submatrix_snap, submatrix_size);
	get_frequencies(logs_values, freq_submatrix_logs, submatrix_size);
	if(!check_frequencies(freq_values_snap, snaps_cant_values)){
		cout << "ERROR! al obtener la frecuencia de los valores unicos sobre los snapshots" << endl;
		return -1;
	}
	if(!check_frequencies(freq_values_logs, logs_cant_values)){
		cout << "ERROR! al obtener la frecuencia de los valores unicos sobre los logs" << endl;
		return -1;
	}
	if(!check_frequencies(freq_submatrix_snap, snaps_cant_submatrix)){
		cout << "ERROR! al obtener la frecuencia de los submatrices unicos sobre los snapshots" << endl;
		return -1;
	}
	if(!check_frequencies(freq_submatrix_logs, logs_cant_submatrix)){
		cout << "ERROR! al obtener la frecuencia de los submatrices unicos sobre los logs" << endl;
		return -1;
	}
	
	sort_by_frequencies(freq_values_snap, freq_sorted_snaps);
	sort_by_frequencies(freq_values_logs, freq_sorted_logs);
	sort_by_frequencies(freq_submatrix_snap, freq_sorted_snaps_submatrix);
	sort_by_frequencies(freq_submatrix_logs, freq_sorted_logs_submatrix);
	
	save_frequencies(freq_values_snap, freq_snapshots_filename);
	save_frequencies(freq_values_logs, freq_logs_filename);
	save_frequencies(freq_sorted_snaps_submatrix, freq_snapshots_filename_submatrix);
	save_frequencies(freq_sorted_logs_submatrix, freq_logs_filename_submatrix);
	
	double entropy_values_snap = get_entropy(freq_values_snap, snaps_cant_values);
	double entropy_values_logs = get_entropy(freq_values_logs, logs_cant_values);
	double entropy_submatrix_snap = get_entropy(freq_submatrix_snap, snaps_cant_submatrix);
	double entropy_submatrix_logs = get_entropy(freq_submatrix_logs, logs_cant_submatrix);
	
	cout << "El tamaño de la palabra es de: " << report.submatrix_size << endl;
	cout << "Encontré " << cant_snapshots << " rasters snapshots y " << cant_logs << " rasters logs" << endl;
	
	cout << "El archivo con las frecuencias de valores snapshots fue guardado en " << freq_snapshots_filename << endl;
	cout << "El archivo con las frecuencias de valores logs fue guardado en " << freq_logs_filename << endl;
	
	cout << "Tengo " << snaps_cant_values << " valores snapshots y " << logs_cant_values << " valores logs" << endl;
	cout << "Entropía de valores snapshots: " << fixed << setprecision(4) << entropy_values_snap << endl;
	cout << "Entropía de valores logs: " << fixed << setprecision(4) << entropy_values_logs << endl;
	
	cout << "10 valores más frecuentes (snaps): " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_sorted_snaps.size())
			cout << freq_sorted_snaps[i].first << ": (" << freq_sorted_snaps[i].second << " apariciones)" << endl;
	}
	
	cout << "10 valores más frecuentes (logs): " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_sorted_logs.size())
			cout << freq_sorted_logs[i].first << ": (" << freq_sorted_logs[i].second << " apariciones)" << endl;
	}
	
	cout << "Tengo " << snaps_cant_submatrix << " submatrices snapshots y " << logs_cant_submatrix << " submatrices logs" << endl;
	
	cout << endl;
	
	cout << "El archivo con las frecuencias de submatrices snapshots fue guardado en " << freq_snapshots_filename_submatrix << endl;
	cout << "El archivo con las frecuencias de submatrices logs fue guardado en " << freq_logs_filename_submatrix << endl;
	
	cout << endl;
	
	cout << "Entropía de submatrices snapshots: " << fixed << setprecision(4) << entropy_submatrix_snap << endl;
	cout << "Entropía de submatrices logs: " << fixed << setprecision(4) << entropy_submatrix_logs << endl;
	
	cout << endl;
	
	
	cout << "10 submatrices más frecuentes (snaps): " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_sorted_snaps_submatrix.size())
			cout << print_submatrix(freq_sorted_snaps_submatrix[i].first) << ": (" << freq_sorted_snaps[i].second << " apariciones)" << endl;
	}
	
	cout << endl;
	
	cout << "10 submatrices más frecuentes (logs): " << endl;
	for(size_t i = 0; i < 10; i++){
		if(i <  freq_sorted_logs_submatrix.size())
			cout << print_submatrix(freq_sorted_logs_submatrix[i].first) << ": (" << freq_sorted_logs[i].second << " apariciones)" << endl;
	}
	
	return 0;
}

