#include <iostream>
#include <vector>
#include <map>

#include "utilities.hpp"

using namespace std;

int main(int argc, char ** argv){
	
	string filename = argv[1];
	
	Report report;
	vector<int> snapshots_values, logs_values;
	if(!get_report_info(filename, snapshots_values, logs_values, report)){
		cout << "EL INFORME CONTIENE ERRORES" << endl;
		return -1;
	}
	
	size_t submatrix_size = report.submatrix_size;
	
	map<int,size_t> freq_values_snap, freq_values_logs;
	map<string, size_t> freq_submatrix_snap, freq_submatrix_logs;
	
	vector<map<int,size_t>> freq_values_snap_per_raster, freq_values_logs_per_raster;
	vector<map<string, size_t>> freq_submatrix_snap_per_raster, freq_submatrix_logs_per_raster;
	
	size_t snaps_cant_values = snapshots_values.size();
	size_t logs_cant_values = logs_values.size();
	size_t snaps_cant_submatrix = snaps_cant_values / submatrix_size;
	size_t logs_cant_submatrix = logs_cant_values / submatrix_size;
	
	get_frequencies(snapshots_values, freq_values_snap);
	get_frequencies(logs_values, freq_values_logs);
	get_frequencies(snapshots_values, freq_submatrix_snap, submatrix_size);
	get_frequencies(logs_values, freq_submatrix_logs, submatrix_size);
	get_frequencies_raster(report, freq_values_snap_per_raster, freq_values_logs_per_raster);
	get_frequencies_raster(report, freq_submatrix_snap_per_raster, freq_submatrix_logs_per_raster, submatrix_size);
	double entropy_values_snap = get_entropy(freq_values_snap, snaps_cant_values);
	double entropy_values_logs = get_entropy(freq_values_logs, logs_cant_values);
	double entropy_submatrix_snap = get_entropy(freq_submatrix_snap, snaps_cant_submatrix);
	double entropy_submatrix_logs = get_entropy(freq_submatrix_logs, logs_cant_submatrix);
	
	size_t size_snap = get_size_representation_global(freq_submatrix_snap, submatrix_size, entropy_values_snap, entropy_submatrix_snap);
	cout << "=======" << endl;
	size_t size_logs = get_size_representation_global(freq_submatrix_logs, submatrix_size, entropy_values_logs, entropy_submatrix_logs);
	cout << "=======" << endl;
	
	size_t size_snap_per_raster = get_size_representation_per_raster(freq_submatrix_snap_per_raster, freq_values_snap_per_raster, submatrix_size);
	cout << "=======" << endl;
	size_t size_logs_per_raster = get_size_representation_per_raster(freq_submatrix_logs_per_raster, freq_values_logs_per_raster, submatrix_size);
	cout << "=======" << endl;
	
	cout << "Tamaño con diccionario global snaps: " << size_snap << " bytes (" << ((double)size_snap / (double)(1024*1024)) << " MB)" << endl;
	cout << "Tamaño con diccionario global logs: " << size_logs << " bytes (" << ((double)size_logs / (double)(1024*1024)) << " MB)" << endl;
	
	cout << endl;
	
	cout << "Tamaño con diccionario por raster snaps: " << size_snap_per_raster << " bytes (" << ((double)size_snap_per_raster / (double)(1024*1024)) << " MB)" << endl;
	cout << "Tamaño con diccionario por raster logs: " << size_logs_per_raster << " bytes (" << ((double)size_logs_per_raster / (double)(1024*1024)) << " MB)" << endl;
	
	cout << endl;
	
	if(size_snap < size_snap_per_raster){
		cout << "Se recomienda utilizar el diccionario global para los snaps" << endl;
	}else{
		cout << "Se recomienda utilizar el diccionario por raster para los snaps" << endl;
	}
	
	if(size_logs < size_logs_per_raster){
		cout << "Se recomienda utilizar el diccionario global para los logs" << endl;
	}else{
		cout << "Se recomienda utilizar el diccionario por raster para los logs" << endl;
	}
	
	return 0;
}