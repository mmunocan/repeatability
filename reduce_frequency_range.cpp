#include <iostream>
#include <string>
#include <fstream>
#include <utility>
#include <cmath>
#include <vector>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 3){
		cout<<"usage: " << argv[0] << " <filename> <range> "<<endl;
		return -1;
	}
	
	
	string filename = argv[1];
	size_t range = atoi(argv[2]);
	
	ifstream file(filename);
	if(!file.is_open()){
		cout <<  filename << " can not be opened" << endl;
		return -1;
	}
	
	vector<pair<int, size_t>> input_freqs;
	int value;
	size_t freq;
	
	while(file >> value >> freq){
		input_freqs.push_back(make_pair(value, freq));
	}
	
	size_t size = input_freqs.size();
	int minimum = input_freqs[0].first < 0 ? input_freqs[0].first : 0;
	int maximum = input_freqs[size-1].first;
	
	size_t size_reduction = ceil((double)(maximum - minimum) / (double)range);
	
	vector<pair<string, size_t>> output;
	string label;
	for(size_t i = 0; i < size_reduction; i++){
		label = "[" + to_string(i*range) + "-" + to_string((i+1)*range-1) + "]";
		output.push_back(make_pair(label, 0));
	}
	
	size_t pos;
	for(size_t i = 0; i < size; i++){
		pos = floor((double)input_freqs[i].first / (double)range);
		output[pos].second += input_freqs[i].second;
	}
	
	for(size_t i = 0; i < size_reduction; i++){
		cout << output[i].first << " " << output[i].second << endl;
	}
	
	return 0;
}