#!/usr/bin/bash

PATH_INPUT="../k2raster/dataset/NASA/"
#PATH_INPUT="../k2raster-2022/dataset/temporal/Temporal/NASA/NLDAS_FORA0125_H/"
PATH_OUTPUT="reports/rasters/"
RASTER=2664

echo "Reporte de los datasets originales:"

./raster_report_global $PATH_INPUT"DLWRF_NLDAS_FORA0125_H.txt" $PATH_INPUT $RASTER $PATH_OUTPUT"DLWRF_FREQ_VAL.txt" $PATH_OUTPUT"DLWRF_FREQ_SUB.txt" > $PATH_OUTPUT"resultados_globales.txt"
echo $PATH_OUTPUT"DLWRF_FREQ_VAL.txt Listo!" 

./raster_report_global $PATH_INPUT"PRES_NLDAS_FORA0125_H.txt" $PATH_INPUT $RASTER $PATH_OUTPUT"PRES_FREQ_VAL.txt" $PATH_OUTPUT"PRES_FREQ_SUB.txt" >> $PATH_OUTPUT"resultados_globales.txt"
echo $PATH_OUTPUT"PRES_FREQ_SUB.txt Listo!" 

./raster_report_global $PATH_INPUT"SPFH_NLDAS_FORA0125_H.txt" $PATH_INPUT $RASTER $PATH_OUTPUT"SPFH_FREQ_VAL.txt" $PATH_OUTPUT"SPFH_FREQ_SUB.txt" >> $PATH_OUTPUT"resultados_globales.txt"
echo $PATH_OUTPUT"SPFH_FREQ_SUB.txt Listo!" 

echo "Reducción de las listas de frecuencias..."

./reduce_frequency_range $PATH_OUTPUT"DLWRF_FREQ_VAL.txt" 1000 > $PATH_OUTPUT"DLWRF_RED_VAL.txt"
echo $PATH_OUTPUT"DLWRF_RED_VAL.txt Listo!"
./reduce_frequency_range $PATH_OUTPUT"PRES_FREQ_VAL.txt" 200000 > $PATH_OUTPUT"PRES_RED_VAL.txt"
echo $PATH_OUTPUT"PRES_RED_VAL.txt Listo!"

