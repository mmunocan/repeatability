#!/usr/bin/bash

echo "Reporte de las estructuras del último nivel (conf: k1=4, k2=2, n1=4, pl=2): "

echo "Generación de las gráficas de frecuencias DLWRF..."
echo "-- Snaps:"

I=0
NOMBRE_RED="reports/last_level_4242/DLWRF/snaps"$I"_red.txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/DLWRF/snaps"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "snaps"$I"_red.txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/DLWRF/snaps"$I"_red.txt"
done

echo "-- Logs:"

I=0
NOMBRE_RED="reports/last_level_4242/DLWRF/logs"$I"_red.txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/DLWRF/logs"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "logs"$I"_red.txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/DLWRF/logs"$I"_red.txt"
done

echo "Listo!"

echo "Generación de las gráficas de frecuencias PRES..."
echo "-- Snaps:"

I=0
NOMBRE_RED="reports/last_level_4242/PRES/snaps"$I"_red.txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/PRES/snaps"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "snaps"$I"_red.txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/PRES/snaps"$I"_red.txt"
done

echo "-- Logs:"

I=0
NOMBRE_RED="reports/last_level_4242/PRES/logs"$I"_red.txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/PRES/logs"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "logs"$I"_red.txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/PRES/logs"$I"_red.txt"
done

echo "Listo!"

echo "Generación de las gráficas de frecuencias SPFH..."
echo "-- Snaps:"

I=0
NOMBRE_RED="reports/last_level_4242/SPFH/snaps"$I".txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/SPFH/snaps"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "snaps"$I".txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/SPFH/snaps"$I".txt"
done

echo "-- Logs:"

I=0
NOMBRE_RED="reports/last_level_4242/SPFH/logs"$I".txt"
while [ -f $NOMBRE_RED  ]
do
	NOMBRE_GRAPHIC="reports/last_level_4242/SPFH/logs"$I".png"
	python3 grafica-frecuencia.py $NOMBRE_RED "logs"$I".txt" $NOMBRE_GRAPHIC 0
	echo $NOMBRE_GRAPHIC" listo!"
	((I++))
	NOMBRE_RED="reports/last_level_4242/SPFH/logs"$I".txt"
done

echo "Listo!"