/*
 * Calcula S_k con k = 1..10 sobre los rasters en plano
 * La información se separa entre S_k de celdas y de submatrices
 */

#include <iostream>
#include <vector>

#include "utilities.hpp"

#define N 10
#define SIDE 8

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> "<<endl;
		return -1;
	}
	
	/*************************************
	 * RECEPCIÓN DE PARAMETROS DE ENTRADA
	 *************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	
	vector<int> values;
	if(!get_raster_zorder(inputs_file, inputs_path, n_elements, values)){
		cout << "ERROR! DURANTE LA LECTURA DE LOS ARCHIVOS DE ENTRADA" << endl;
		return -1;
	}
	
	/*************************************************************
	 * TRANSFORMACIÓN DE LOS VALORES EN CADENAS DE BYTES (O CHAR)
	 *************************************************************/
	char * values_char = (char*)values.data();
	size_t values_char_size = values.size() * sizeof(int);
	
	/***************************
	 * DEFINICIÓN DE VARIABLES
	 ***************************/
	size_t word_size_values = sizeof(int);
	size_t word_size_submatrix = SIDE * SIDE * word_size_values;
	size_t cant_values = values.size();
	size_t cant_submatrices = cant_values / (SIDE*SIDE);
	
	size_t sk;
	
	/*******************************
	 * CÁLCULO DEL DELTA DE VALORES
	 *******************************/
	
	cout << endl;
	cout << "DELTA DE VALORES" << endl;
	cout << endl;
	cout << "k & valor \\\\ \\hline " << endl;
	
	for(size_t k = 1; k <= N; k++){
		sk = S_k(values_char, values_char_size, word_size_values, k);
		cout << k << " & " << sk << " \\\\ \\hline " << endl;
	}
	
	/***********************************
	 * CÁLCULO DEL DELTA DE SUBMATRICES
	 ***********************************/
	
	cout << endl;
	cout << "DELTA DE SUBMATRICES" << endl;
	cout << endl;
	cout << "k & valor \\\\ \\hline " << endl;
	
	for(size_t k = 1; k <= N; k++){
		sk = S_k(values_char, values_char_size, word_size_submatrix, k);
		cout << k << " & " << sk << " \\\\ \\hline " << endl;
	}
	
	return 0;
}