/*
 * Dado un informe generado tras la construcción de un Tk2-raster, calcula S_k con k = 1..10
 * La información se separa entre los rasters snapshots y rasters logs, como también entre S_k de celdas y de submatrices
 */

#include <iostream>
#include <vector>

#include "utilities.hpp"

#define N 10

using namespace std;

int main(int argc, char ** argv){
	if(argc != 2){
		cout << "ERROR! use " << argv[0] << " <FILENAME> " << endl;
		return -1;
	}
	
	/*************************************
	 * RECEPCIÓN DE PARAMETROS DE ENTRADA
	 *************************************/
	string filename = argv[1];
	
	/***************************
	 * DEFINICIÓN DE VARIABLES
	 ***************************/
	Report report;									// Reporte estructurado
	vector<int> snapshots_values, logs_values;		// Valores de plains_levels
	char * snapshots_words, * logs_words;			// Valores disgregados en bytes (o char)
	
	/********************************
	 * LECTURA Y CHEQUEO DEL INFORME
	 ********************************/
	if(!get_report_info(filename, snapshots_values, logs_values, report)){
		cout << "EL INFORME CONTIENE ERRORES" << endl;
		return -1;
	}
	
	/*************************************************************
	 * TRANSFORMACIÓN DE LOS VALORES EN CADENAS DE BYTES (O CHAR)
	 *************************************************************/
	snapshots_words = (char*) snapshots_values.data();
	logs_words = (char*) logs_values.data();
	
	/********************************************
	 * OBTENNCION DE PRIMEROS DATOS ESTADISTICOS
	 ********************************************/
	
	// Cantidad de celdas en plains_levels
	size_t snapshots_values_size = snapshots_values.size();
	size_t logs_values_size = logs_values.size();
	
	// Cantidad de bytes (o char) en plains_levels
	size_t snapshots_words_size = snapshots_values_size * sizeof(int);
	size_t logs_words_size = logs_values_size * sizeof(int);
	
	// Tamaño de una submatriz en bytes (o char)
	size_t word_size = report.submatrix_size * sizeof(int);
	
	size_t snapshots_values_s_1, logs_values_s_1, snapshots_submatrix_s_1, logs_submatrix_s_1;
	
	/*******************************
	 * CÁLCULO DEL DELTA DE VALORES
	 *******************************/
	
	cout << endl;
	cout << "DELTA DE VALORES" << endl;
	cout << endl;
	cout << "k & Snapshot & Logs \\\\ \\hline " << endl;
	
	for(size_t k = 1; k <= N; k++){
		snapshots_values_s_1 = S_k(snapshots_words, snapshots_words_size, sizeof(int), k);
		logs_values_s_1 = S_k(logs_words, logs_words_size, sizeof(int), k);
		cout << k << " & " << snapshots_values_s_1 << " & " << logs_values_s_1 << " \\\\ \\hline " << endl;
	}
	
	/***********************************
	 * CÁLCULO DEL DELTA DE SUBMATRICES
	 ***********************************/
	
	cout << endl;
	cout << "DELTA DE SUBMATRICES" << endl;
	cout << endl;
	cout << "k & Snapshot & Logs \\\\ \\hline " << endl;
	
	for(size_t k = 1; k <= N; k++){
		snapshots_submatrix_s_1 = S_k(snapshots_words, snapshots_words_size, word_size, k);
		logs_submatrix_s_1 = S_k(logs_words, logs_words_size, word_size, k);
		cout << k <<" & " << snapshots_submatrix_s_1 << " & " << logs_submatrix_s_1 << " \\\\ \\hline " << endl;
	}
	
	return 0;
}