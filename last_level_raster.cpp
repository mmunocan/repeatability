/*
 * Dado un informe generado tras la construcción de un Tk2-raster, realiza un resumen DEL PROMEDIO DE VALORES
 * de las submatrices ubicadas en 'plains_levels' 
 * de los rasters generados.
 * La información se separa entre los rasters snapshots y rasters logs
 */

#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#include "utilities.hpp"

using namespace std;

int main(int argc, char ** argv){
	if(argc != 3){
		cout<<"usage: " << argv[0] << " <filename> <details_path>"<<endl;
		return -1;
	}
	
	string filename = argv[1];
	string details_path = argv[2];
	
	Report report;
	vector<int> snapshots_values, logs_values;
	if(!get_report_info(filename, snapshots_values, logs_values, report)){
		cout << "EL INFORME CONTIENE ERRORES" << endl;
		return -1;
	}
	
	size_t submatrix_size = report.submatrix_size;
	size_t cant_snapshots = report.n_snapshots;
	size_t cant_logs = report.n_logs;
	
	vector<map<int,size_t>> freq_values_snap, freq_values_logs;
	vector<map<string, size_t>> freq_submatrix_snap, freq_submatrix_logs;
	
	get_frequencies_raster(report, freq_values_snap, freq_values_logs);
	get_frequencies_raster(report, freq_submatrix_snap, freq_submatrix_logs, submatrix_size);
	
	
	double entropy_values_snap = get_entropy_average(freq_values_snap);
	double entropy_values_logs = get_entropy_average(freq_values_logs);
	double entropy_submatrix_snap = get_entropy_average(freq_submatrix_snap);
	double entropy_submatrix_logs = get_entropy_average(freq_submatrix_logs);
	
	size_t uniques_values_snap = get_uniques_values_average(freq_values_snap);
	size_t uniques_values_logs = get_uniques_values_average(freq_values_logs);
	size_t uniques_submatrix_snap = get_uniques_values_average(freq_submatrix_snap);
	size_t uniques_submatrix_logs = get_uniques_values_average(freq_submatrix_logs);
	
	string output_filename = details_path + "snaps";
	save_frequencies(freq_values_snap, output_filename);
	output_filename = details_path + "logs";
	save_frequencies(freq_values_logs, output_filename);
	
	cout << "Se generaron los informes detallados" << endl;
	
	cout << "El tamaño de la palabra es de: " << report.submatrix_size << endl;
	cout << "Encontré " << cant_snapshots << " rasters snapshots y " << cant_logs << " rasters logs" << endl;
	
	cout << "Entropía promedio de valores snapshots: " << fixed << setprecision(4) << entropy_values_snap << endl;
	cout << "Entropía promedio de valores logs: " << fixed << setprecision(4) << entropy_values_logs << endl;
	cout << "Entropía promedio de submatrices snapshots: " << fixed << setprecision(4) << entropy_submatrix_snap << endl;
	cout << "Entropía promedio de submatrices logs: " << fixed << setprecision(4) << entropy_submatrix_logs << endl;
	
	cout << "Valores únicos promedio de valores snapshots: " << fixed << setprecision(4) << uniques_values_snap << endl;
	cout << "Valores únicos promedio de valores logs: " << fixed << setprecision(4) << uniques_values_logs << endl;
	cout << "Valores únicos promedio de submatrices snapshots: " << fixed << setprecision(4) << uniques_submatrix_snap << endl;
	cout << "Valores únicos promedio de submatrices logs: " << fixed << setprecision(4) << uniques_submatrix_logs << endl;
	
	return 0;
}