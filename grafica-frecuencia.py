import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

nombres = []
freq = []
with open(sys.argv[1]) as fname:
	for lineas in fname:
		dataset = lineas.split()
		nombres.append((dataset[0]))
		freq.append(int(dataset[1]))


fig, ax = plt.subplots()

ax.set_ylabel('Frequencies (log scale)')
ax.set_xlabel('Values')
ax.set_title(sys.argv[2])
ax.set_yscale('log')

plt.xticks(rotation=90)
if(int(sys.argv[4]) == 0):
	plt.bar(nombres, freq)
else:
	n=50 if 50 < len(freq) else len(freq)
	plt.bar(np.arange(n), freq[0:n])
fig.set_size_inches(15, 10)
plt.tight_layout()
plt.savefig(sys.argv[3])
