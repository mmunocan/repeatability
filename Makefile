CPP=g++

OBJECTS=utilities.o

BINS=raster_report_global last_level_global reduce_frequency_range last_level_raster get_diccionary_size
		
CPPFLAGS=-std=c++11 -O3 -DNDEBUG
DEST=.

%.o: %.c
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)

raster_report_global:
	$(CPP) -o $(DEST)/raster_report_global raster_report_global.cpp $(OBJECTS) $(CPPFLAGS)
	
last_level_global:
	$(CPP) -o $(DEST)/last_level_global last_level_global.cpp $(OBJECTS) $(CPPFLAGS)
	
reduce_frequency_range:
	$(CPP) -o $(DEST)/reduce_frequency_range reduce_frequency_range.cpp $(OBJECTS) $(CPPFLAGS)

last_level_raster:
	$(CPP) -o $(DEST)/last_level_raster last_level_raster.cpp $(OBJECTS) $(CPPFLAGS)

get_diccionary_size:
	$(CPP) -o $(DEST)/get_diccionary_size get_diccionary_size.cpp $(OBJECTS) $(CPPFLAGS)

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f *.a $(BINS)
