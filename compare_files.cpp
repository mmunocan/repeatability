#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 3){
		cout<<"usage: " << argv[0] << " <filename1> <filename2>  "<<endl;
		return -1;
	}
	
	string filename1 = argv[1];
	string filename2 = argv[2];
	
	ifstream file1(filename1);
	ifstream file2(filename2);
	
	char c1, c2;
	bool correcto = true;
	
	
	while(file1.get(c1) && file2.get(c2)){
		if(c1 != c2){
			cout << "Caracteres diferentes" << endl;
			correcto = false;
			break;
		}
	}
	file1.get(c1);
	file2.get(c2);
	if(c1 != c2){
		cout << "Caracteres diferentes" << endl;
		correcto = false;
	}
	
	if(!file1.eof() || !file2.eof()){
		cout << "Tamaños diferentes" << endl;
		if(file1.eof()){
			cout << "Error en el archivo 1: ";
			while(file1.get(c1))
				cout << (int)c1;
			cout << endl;
		}
		if(file2.eof()){
			cout << "Error en el archivo 2: ";
			while(file2.get(c2))
				cout << (int)c2;
			cout << endl;
		}
		correcto = false;
	}
	
	if(correcto){
		cout << "Los archivos son iguales!" << endl;
	}else{
		cout << "Los archivos no son iguales!" << endl;
	}
	
	return 0;
}