#!/usr/bin/bash

echo "Compilacion del código..."

make

echo "Borrado de antiguas carpetas"
rm -r reports

echo "Creación de nuevas carpetas"

mkdir reports
mkdir reports/rasters
mkdir reports/rasters/DLWRF
mkdir reports/rasters/PRES
mkdir reports/rasters/SPFH
mkdir reports/last_level_2222
mkdir reports/last_level_2222/DLWRF
mkdir reports/last_level_2222/PRES
mkdir reports/last_level_2222/SPFH
mkdir reports/last_level_2221
mkdir reports/last_level_2221/DLWRF
mkdir reports/last_level_2221/PRES
mkdir reports/last_level_2221/SPFH
mkdir reports/last_level_4242
mkdir reports/last_level_4242/DLWRF
mkdir reports/last_level_4242/PRES
mkdir reports/last_level_4242/SPFH

echo "Listo!"