#!/usr/bin/bash

PATH_OUTPUT="reports/rasters/"

echo "Generación de las gráficas de valores"

python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_RED_VAL.txt" "DLWRF raster values frequencies" $PATH_OUTPUT"DLWRF.png" 0
echo $PATH_OUTPUT"DLWRF.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_RED_VAL.txt" "PRES raster values frequencies" $PATH_OUTPUT"PRES.png" 0
echo $PATH_OUTPUT"PRES.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_FREQ_VAL.txt" "SPFH raster values frequencies" $PATH_OUTPUT"SPFH.png" 0
echo $PATH_OUTPUT"SPFH.png Listo!"

echo "Generación de las gráficas de submatrices"

python3 grafica-frecuencia.py $PATH_OUTPUT"DLWRF_FREQ_SUB.txt" "DLWRF raster 50° most frequencies submatrices" $PATH_OUTPUT"DLWRF_SUB.png" 1
echo $PATH_OUTPUT"DLWRF_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"PRES_FREQ_SUB.txt" "PRES raster 50° most frequencies submatrices" $PATH_OUTPUT"PRES_SUB.png" 1
echo $PATH_OUTPUT"PRES_SUB.png Listo!"
python3 grafica-frecuencia.py $PATH_OUTPUT"SPFH_FREQ_SUB.txt" "SPFH raster 50° most frequencies submatrices" $PATH_OUTPUT"SPFH_SUB.png" 1
echo $PATH_OUTPUT"SPFH_SUB.png Listo!"

