/* 
 * Tras la lectura de los rasters en plano, obtiene cierta información de relevancia
 * relativa a la frecuencia de valores, submatrices y entropías
 */

#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#include "utilities.hpp"

#define SIDE 8

using namespace std;

int main(int argc, char ** argv){
	if(argc != 4){
		cout<<"usage: " << argv[0] << " <inputs_file> <inputs_path> <n_rasters> "<<endl;
		return -1;
	}
	
	/*************************************
	 * RECEPCIÓN DE PARAMETROS DE ENTRADA
	 *************************************/
	string inputs_file = argv[1];
    string inputs_path = argv[2];
	size_t n_elements = atoi(argv[3]);
	
	vector<vector<int>> values;
	if(!get_separate_rasters_zorder(inputs_file, inputs_path, n_elements, values)){
		cout << "ERROR! DURANTE LA LECTURA DE LOS ARCHIVOS DE ENTRADA" << endl;
		return -1;
	}
	
	size_t values_per_raster = values[0].size();
	size_t submatrix_per_raster = values_per_raster / (SIDE * SIDE);
	size_t values_char_size = values_per_raster * sizeof(int);
	size_t word_size_values = sizeof(int);
	size_t word_size_submatrix = SIDE * SIDE * word_size_values;
	
	char * raster_values_char;
	
	size_t values_uniques = 0;
	size_t submatrix_uniques = 0;
	size_t values_max_freq = 0;
	size_t submatrix_max_freq = 0;
	size_t values_freq_ones = 0;
	size_t submatrix_freq_ones = 0;
	double values_entropy = 0.0;
	double submatrix_entropy = 0.0;
	
	/*****************************
	 * REVISIÓN RASTER POR RASTER
	 *****************************/
	for(size_t i = 0; i < n_elements; i++){
		map<string,size_t> freq_values, freq_submatrix;
		raster_values_char = (char*) values[i].data();
		
		get_frequencies(freq_values, raster_values_char, word_size_values, values_char_size);
		get_frequencies(freq_submatrix, raster_values_char, word_size_submatrix, values_char_size);
		
		values_uniques += freq_values.size();
		submatrix_uniques += freq_submatrix.size();
		values_max_freq += highter_frequency(freq_values);
		submatrix_max_freq += highter_frequency(freq_submatrix);
		values_freq_ones += how_many_freq_one(freq_values);
		submatrix_freq_ones += how_many_freq_one(freq_submatrix);
		values_entropy += get_entropy(freq_values, values_per_raster);
		submatrix_entropy += get_entropy(freq_submatrix, submatrix_per_raster);
	}
	
	values_uniques /= n_elements;
	submatrix_uniques /= n_elements;
	values_max_freq /= n_elements;
	submatrix_max_freq /= n_elements;
	values_freq_ones /= n_elements;
	submatrix_freq_ones /= n_elements;
	values_entropy /= n_elements;
	submatrix_entropy /= n_elements;
	
	/**************************
	 * GENERACION DEL INFORME
	 **************************/
	cout << fixed << setprecision(4);
	double percentaje;
	cout << "Tamaño de la submatriz: " << SIDE*SIDE  << " celdas" << endl;
	cout << "LOS SIGUIENTES SON VALORES PROMEDIOS POR RASTER Y TIPO" << endl;
	cout << "===============================================================" << endl;
	cout << "Datos & Valor " << endl;
	cout << "Cantidad de celdas por raster & " << values_per_raster << endl;
	percentaje = (double)values_uniques / values_per_raster * 100;
	cout << "Celdas únicas promedio & " << values_uniques << "(" << percentaje << "%)" << endl;
	percentaje = (double)values_max_freq / values_per_raster * 100;
	cout << "Frecuencia máxima promedio de celdas & " << values_max_freq << "(" << percentaje << "%)" << endl;
	percentaje = (double)values_freq_ones / values_per_raster * 100;
	cout << "Cantidad promedio de celdas con frecuencia 1 & " << values_freq_ones << "(" << percentaje << "%)" << endl;
	cout << "Entropía promedio de celdas & " << values_entropy << endl;
	cout << "Espacio mínimo de codificación de celdas [MB]  & " << (n_elements*values_entropy*values_per_raster)/(8*1024*1024) << endl;
	cout << "===============================================================" << endl;
	cout << "Cantidad de submatrices por raster & " << submatrix_per_raster << endl;
	percentaje = (double)submatrix_uniques / submatrix_per_raster * 100;
	cout << "Submatrices únicas promedio & " << submatrix_uniques << "(" << percentaje << "%)" << endl;
	percentaje = (double)submatrix_max_freq / submatrix_per_raster * 100;
	cout << "Frecuencia máxima promedio de submatrices & " << submatrix_max_freq << "(" << percentaje << "%)" << endl;
	percentaje = (double)submatrix_freq_ones / submatrix_per_raster * 100;
	cout << "Cantidad promedio de submatrices con frecuencia 1 & " << submatrix_freq_ones << "(" << percentaje << "%)" << endl;
	cout << "Entropía promedio de submatrices & " << submatrix_entropy << endl;
	cout << "Espacio mínimo de codificación de submatrices [MB]  & " << (n_elements*submatrix_entropy*submatrix_per_raster)/(8*1024*1024) << endl;
	cout << "===============================================================" << endl;
	
	return 0;
}